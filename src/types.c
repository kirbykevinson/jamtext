#include <stdarg.h>
#include <string.h>

#include "jamtext-internal.h"

void jam_free(struct jam_value *value) {
	if (value == NULL) {
		return;
	}
	
	switch (value->type) {
	case JAM_VALUE_STRING:
		free(value->contents.as_string);
		
		break;
	case JAM_VALUE_LIST: {
		struct jam_list *list = value->contents.as_list;
		
		while (true) {
			struct jam_list *next = NULL;
			
			if (list != NULL) {
				jam_free(list->value);
				
				next = list->next;
			}
			
			free(list);
			
			if (next == NULL) {
				break;
			}
			
			list = next;
		}
		
		break;
	}
	case JAM_VALUE_OBJECT: {
		struct jam_object *object = value->contents.as_object;
		
		while (true) {
			struct jam_object *next = NULL;
			
			if (object != NULL) {
				free(object->key);
				jam_free(object->value);
				
				next = object->next;
			}
			
			free(object);
			
			if (next == NULL) {
				break;
			}
			
			object = next;
		}
		
		break;
	}
	default:
		break;
	}
	
	free(value->flavor);
	free(value);
}

struct jam_value *jam_null_new(const char *flavor) {
	struct jam_value *output = malloc(sizeof(struct jam_value));
	
	if (output == NULL) {
		return NULL;
	}
	
	output->type = JAM_VALUE_NULL;
	output->flavor = strdup(flavor);
	
	return output;
}

struct jam_value *jam_bool_new(const char *flavor, bool value) {
	struct jam_value *output = malloc(sizeof(struct jam_value));
	
	if (output == NULL) {
		return NULL;
	}
	
	output->type = JAM_VALUE_BOOL;
	output->flavor = strdup(flavor);
	output->contents.as_bool = value;
	
	return output;
}

struct jam_value *jam_number_new(const char *flavor, double value) {
	struct jam_value *output = malloc(sizeof(struct jam_value));
	
	if (output == NULL) {
		return NULL;
	}
	
	output->type = JAM_VALUE_NUMBER;
	output->flavor = strdup(flavor);
	output->contents.as_number = value;
	
	return output;
}

struct jam_value *jam_string_new(const char *flavor, const char *value) {
	struct jam_value *output = malloc(sizeof(struct jam_value));
	
	if (output == NULL) {
		return NULL;
	}
	
	output->type = JAM_VALUE_STRING;
	output->flavor = strdup(flavor);
	output->contents.as_string = strdup(value);
	
	return output;
}

struct jam_value *jam_list_new(const char *flavor, ...) {
	va_list args;
	
	struct jam_value *output = malloc(sizeof(struct jam_value));
	
	if (output == NULL) {
		return NULL;
	}
	
	output->type = JAM_VALUE_LIST;
	output->flavor = strdup(flavor);
	output->contents.as_list = NULL;
	
	va_start(args, flavor);
	
	struct jam_list **current_list = &output->contents.as_list;
	struct jam_value *element = NULL;
	
	for (
		size_t i = 0;
		(element = va_arg(args, struct jam_value *)) != NULL;
		i++
	) {
		*current_list = malloc(sizeof(struct jam_list));
		
		if (*current_list == NULL) {
			va_end(args);
			
			jam_free(output);
			
			return NULL;
		}
		
		(*current_list)->value = element;
		(*current_list)->length = 0;
		(*current_list)->next = NULL;
		(*current_list)->last = NULL;
		
		output->contents.as_list->length = i + 1;
		output->contents.as_list->last = *current_list;
		
		current_list = &(*current_list)->next;
	}
	
	va_end(args);
	
	return output;
}

struct jam_value *jam_object_new(const char *flavor, ...) {
	va_list args;
	
	struct jam_value *output = malloc(sizeof(struct jam_value));
	
	if (output == NULL) {
		return NULL;
	}
	
	output->type = JAM_VALUE_OBJECT;
	output->flavor = strdup(flavor);
	output->contents.as_object = NULL;
	
	va_start(args, flavor);
	
	struct jam_object **current_object = &output->contents.as_object;
	char *key = NULL;
	
	for (size_t i = 0; (key = va_arg(args, char *)) != NULL; i++) {
		*current_object = malloc(sizeof(struct jam_object));
		
		if (*current_object == NULL) {
			va_end(args);
			
			jam_free(output);
			
			return NULL;
		}
		
		(*current_object)->key = strdup(key);
		(*current_object)->key_length = strlen(key);
		(*current_object)->value = va_arg(args, struct jam_value *);
		(*current_object)->next = NULL;
		
		current_object = &(*current_object)->next;
	}
	
	va_end(args);
	
	return output;
}

struct jam_value *jam_list_get(
	const struct jam_value *value,
	size_t index
) {
	if (index < 0 || index >= value->contents.as_list->length) {
		return NULL;
	}
	
	size_t i = 0;
	struct jam_list *list = NULL;
	
	jam_list_foreach (i, list, value) {
		if (i == index) {
			return list->value;
		}
	}
	
	return NULL;
}
bool jam_list_push(struct jam_value *value, struct jam_value *to_push) {
	if (value->contents.as_list == NULL) {
		value->contents.as_list = malloc(sizeof(struct jam_list));
		
		if (value->contents.as_list == NULL) {
			return false;
		}
		
		value->contents.as_list->value = to_push;
		value->contents.as_list->length = -1;
		value->contents.as_list->next = NULL;
		value->contents.as_list->last = value->contents.as_list;
	} else {
		struct jam_list *last = value->contents.as_list->last;
		
		last->next = malloc(sizeof(struct jam_list));
		
		if (last->next == NULL) {
			return false;
		}
		
		last->next->value = to_push;
		last->next->length = -1;
		last->next->next = NULL;
		last->next->last = NULL;
		
		value->contents.as_list->last = last->next;
	}
	
	value->contents.as_list->length++;
	
	return true;
}

struct jam_value *jam_object_get(
	const struct jam_value *value,
	const char *key
) {
	size_t key_length = strlen(key);
	
	struct jam_object *object = NULL;
	
	jam_object_foreach (object, value) {
		if (
			object->key_length == key_length &&
			strcmp(object->key, key) == 0
		) {
			return object->value;
		}
	}
	
	return NULL;
}
bool jam_object_set(
	struct jam_value *value,
	const char *key,
	struct jam_value *to_set
) {
	struct jam_object **object = &value->contents.as_object;
	
	while (*object != NULL) {
		if (strcmp((*object)->key, key) == 0) {
			(*object)->value = to_set;
			
			return true;
		}
		
		object = &(*object)->next;
	}
	
	*object = malloc(sizeof(struct jam_object));
	
	if (*object == NULL) {
		return false;
	}
	
	(*object)->key = strdup(key);
	(*object)->key_length = strlen(key);
	(*object)->value = to_set;
	(*object)->next = NULL;
	
	return true;
}
