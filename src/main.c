#include "jamtext-internal.h"

const int
	JAM_VERSION_MAJOR = 1,
	JAM_VERSION_MINOR = 0,
	JAM_VERSION_PATCH = 0;

size_t jam_nesting_limit = 127;
