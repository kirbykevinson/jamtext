#ifndef JAMTEXT_INTERNAL_H
#define JAMTEXT_INTERNAL_H

#include <stdint.h>

#include "jamtext.h"

enum jam_processor_type {
	JAM_PROCESSOR_FILE,
	JAM_PROCESSOR_BUFFER
};

struct jam_serializer {
	enum jam_processor_type type;
	
	FILE *file;
	char **buffer;
	
	size_t buffer_size, buffer_used;
	
	const char *error;
};

struct jam_deserializer {
	enum jam_processor_type type;
	
	FILE *file;
	const char *buffer;
	
	size_t cursor, buffer_size;
	
	size_t line, column;
	int prev_char, current_char;
	
	const char *error;
};

typedef int (*jam_printfn_t)(void *, const char *, ...);

void jam_grow_string(char **, size_t *, size_t *, size_t);
bool jam_print_number(
	jam_printfn_t printfn,
	void *stream,
	double number
);
double jam_strtod(const char *, char **);
int jam_utf8_encode(char *, uint32_t);

int jam_next_char(struct jam_deserializer *);

enum jam_char {
	JAM_CHAR_NULL = '\0',
	JAM_CHAR_EOF = EOF,
	JAM_CHAR_SPACE = ' ',
	JAM_CHAR_TAB = '\t',
	JAM_CHAR_LF = '\n',
	JAM_CHAR_CR = '\r',
	JAM_CHAR_COMMENT_START = '#',
	JAM_CHAR_QUOTE = '"',
	JAM_CHAR_LIST_START = '[',
	JAM_CHAR_LIST_END = ']',
	JAM_CHAR_OBJECT_START = '{',
	JAM_CHAR_OBJECT_END = '}',
	JAM_CHAR_COMMA = ',',
	JAM_CHAR_COLON = ':',
	JAM_CHAR_BACKSLASH = '\\',
	JAM_CHAR_ESCAPE_FROM_BACKSPACE = 'b',
	JAM_CHAR_ESCAPE_FROM_FORM_FEED = 'f',
	JAM_CHAR_ESCAPE_FROM_LF = 'n',
	JAM_CHAR_ESCAPE_FROM_CR = 'r',
	JAM_CHAR_ESCAPE_FROM_TAB = 't',
	JAM_CHAR_ESCAPE_FROM_UNICODE = 'u'
};

extern const char
	*JAM_WORD_NULL,
	*JAM_WORD_TRUE,
	*JAM_WORD_FALSE,
	
	*JAM_WORD_CHILDREN,
	
	*JAM_SPECIAL_CHARS;

enum jam_token_type {
	JAM_TOKEN_INVALID,
	JAM_TOKEN_EOF,
	JAM_TOKEN_FLAVOR,
	JAM_TOKEN_NULL,
	JAM_TOKEN_BOOL,
	JAM_TOKEN_NUMBER,
	JAM_TOKEN_STRING,
	JAM_TOKEN_LIST_START,
	JAM_TOKEN_LIST_END,
	JAM_TOKEN_OBJECT_START,
	JAM_TOKEN_OBJECT_END,
	JAM_TOKEN_COMMA,
	JAM_TOKEN_COLON
};

union jam_token_contents {
	bool as_bool;
	double as_number;
	char *as_string;
};

struct jam_token {
	enum jam_token_type type;
	
	union jam_token_contents contents;
};

struct jam_token jam_next_token(struct jam_deserializer *);

void jam_print_token(struct jam_token);

struct jam_value *jam_get_value(struct jam_deserializer *);

#endif
