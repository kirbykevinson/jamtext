#include <math.h>
#include <stdarg.h>
#include <string.h>

#include "jamtext-internal.h"

#define die(message)\
	serializer->error = message;\
	return false;

#define print(...)\
	if (serializer_print(serializer, __VA_ARGS__) < 0) {\
		die("out of memory");\
	}

struct jam_serializer *jam_serializer_new(FILE *file) {
	struct jam_serializer *serializer = malloc(sizeof(struct jam_serializer));
	
	if (serializer == NULL) {
		return NULL;
	}
	
	serializer->type = JAM_PROCESSOR_FILE;
	
	serializer->file = file;
	
	serializer->error = "";
	
	return serializer;
}
struct jam_serializer *jam_serializer_new_to_string(char **str) {
	struct jam_serializer *serializer = malloc(sizeof(struct jam_serializer));
	
	if (serializer == NULL) {
		return NULL;
	}
	
	serializer->type = JAM_PROCESSOR_BUFFER;
	
	serializer->buffer = str;
	
	serializer->buffer_size = 0;
	serializer->buffer_used = 0;
	
	serializer->error = "";
	
	return serializer;
}

static int serializer_print(
	struct jam_serializer *serializer,
	const char *format,
	...
) {
	va_list args, args1;
	
	va_start(args, format);
	va_start(args1, format);
	
	int wrote = 0;
	
	if (serializer->type == JAM_PROCESSOR_FILE) {
		wrote = vfprintf(serializer->file, format, args);
	} else {
		// It appears that vsnprint can't count more than INT_MAX
		// characters. Luckily, we don't need so many
		
		size_t to_grow = vsnprintf(NULL, 0, format, args);
		
		jam_grow_string(
			serializer->buffer,
			
			&serializer->buffer_size,
			&serializer->buffer_used,
			
			to_grow
		);
		
		if (*serializer->buffer == NULL) {
			va_end(args);
			va_end(args1);
			
			return -1;
		}
		
		wrote = vsprintf(
			*serializer->buffer + serializer->buffer_used,
			format, args1
		);
		
		serializer->buffer_used += to_grow;
	}
	
	va_end(args);
	va_end(args1);
	
	return wrote;
}

static bool serialize_string(
	struct jam_serializer *serializer,
	const char *str
) {
	print("%c", JAM_CHAR_QUOTE);
	
	for (size_t i = 0; str[i] != '\0'; i++) {
		switch (str[i]) {
		case '"':
			print("%c%c", JAM_CHAR_BACKSLASH, JAM_CHAR_QUOTE);
			
			break;
		case '\\':
			print("%c%c", JAM_CHAR_BACKSLASH, JAM_CHAR_BACKSLASH);
			
			break;
		case '\b':
			print("%c%c", JAM_CHAR_BACKSLASH, JAM_CHAR_ESCAPE_FROM_BACKSPACE);
			
			break;
		case '\f':
			print("%c%c", JAM_CHAR_BACKSLASH, JAM_CHAR_ESCAPE_FROM_FORM_FEED);
			
			break;
		case '\n':
			print("%c%c", JAM_CHAR_BACKSLASH, JAM_CHAR_ESCAPE_FROM_LF);
			
			break;
		case '\r':
			print("%c%c", JAM_CHAR_BACKSLASH, JAM_CHAR_ESCAPE_FROM_CR);
			
			break;
		case '\t':
			print("%c%c", JAM_CHAR_BACKSLASH, JAM_CHAR_ESCAPE_FROM_TAB);
			
			break;
		default:
			if ((unsigned char) str[i] < 32 || (unsigned char) str[i] == 127) {
				print("%c%c%04x",
					JAM_CHAR_BACKSLASH,
					JAM_CHAR_ESCAPE_FROM_UNICODE,
				str[i]);
				
				break;
			}
			
			print("%c", str[i]);
			
			break;
		}
	}
	
	print("%c", JAM_CHAR_QUOTE);
	
	return true;
}

static bool serialize_value(
	struct jam_serializer *serializer,
	const struct jam_value *value,
	size_t nesting_level
) {
	if (nesting_level >= jam_nesting_limit) {
		die("too much nesting");
	}
	
	for (size_t i = 0; value->flavor[i] != '\0'; i++) {
		if (strchr(JAM_SPECIAL_CHARS, value->flavor[i]) != NULL) {
			die("illegal character in the flavor");
		}
		
		print("%c", value->flavor[i]);
	}
	if (value->flavor[0] != '\0') {
		print(" ");
	}
	
	switch (value->type) {
	case JAM_VALUE_NULL:
		print(JAM_WORD_NULL);
		
		break;
	case JAM_VALUE_BOOL:
		if (value->contents.as_bool) {
			print(JAM_WORD_TRUE);
		} else {
			print(JAM_WORD_FALSE);
		}
		
		break;
	case JAM_VALUE_NUMBER:
		if (isnan(value->contents.as_number)) {
			die("unexpected NaN");
		}
		if (isinf(value->contents.as_number)) {
			die("unexpected Infinity");
		}
		
		if (!jam_print_number(
			(jam_printfn_t) serializer_print,
			serializer,
			value->contents.as_number
		)) {
			die("out of memory");
		}
		
		break;
	case JAM_VALUE_STRING:
		if(!serialize_string(
			serializer,
			value->contents.as_string
		)) {
			return false;
		}
		
		break;
	case JAM_VALUE_LIST:
		print("%c", JAM_CHAR_LIST_START);
		
		size_t index = 0;
		struct jam_list *list = NULL;
		
		jam_list_foreach (index, list, value) {
			bool result = serialize_value(
				serializer, list->value, nesting_level + 1
			);
			
			if (!result) {
				return false;
			}
			
			if (list->next != NULL) {
				print("%c ", JAM_CHAR_COMMA);
			}
		}
		
		print("%c", JAM_CHAR_LIST_END);
		
		break;
	case JAM_VALUE_OBJECT:
		print("%c", JAM_CHAR_OBJECT_START);
		
		struct jam_value *children = jam_object_get(value, JAM_WORD_CHILDREN);
		
		struct jam_object *object = NULL;
		
		jam_object_foreach (object, value) {
			if (strcmp(object->key, JAM_WORD_CHILDREN) == 0) {
				continue;
			}
			
			bool result;
			
			result = serialize_string(serializer, object->key);
			
			if (!result) {
				return false;
			}
			
			print("%c ", JAM_CHAR_COLON);
			
			result = serialize_value(
				serializer, object->value, nesting_level + 1
			);
			
			if (!result) {
				return false;
			}
			
			if (object->next != NULL) {
				print("%c ", JAM_CHAR_COMMA);
			}
		}
		
		if (children != NULL) {
			if (children->type != JAM_VALUE_LIST) {
				die("illegal children container type");
			}
			
			size_t index = 0;
			struct jam_list *child = NULL;
			
			jam_list_foreach (index, child, children) {
				bool result;
				
				result = serialize_value(
					serializer, child->value, nesting_level + 1
				);
				
				if (!result) {
					return false;
				}
				
				if (child->next != NULL) {
					print("%c ", JAM_CHAR_COMMA);
				}
			}
		}
		
		print("%c", JAM_CHAR_OBJECT_END);
		
		break;
	default:
		die("invalid object type");
	}
	
	return true;
}

bool jam_serializer_run(
	struct jam_serializer *serializer,
	const struct jam_value *value
) {
	return serialize_value(serializer, value, 0);
}

const char *jam_serializer_get_error(struct jam_serializer *serializer) {
	return serializer->error;
}
