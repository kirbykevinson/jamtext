#include "jamtext-internal.h"

#define die(message)\
	*status = JAM_STATUS_ERROR;\
	\
	jam_free(value);\
	\
	deserializer->error = message;\
	\
	return NULL;

#define memcheck\
	if (flavor[0] != '\0'){\
		free(flavor);\
		\
		flavor = "";\
	}\
	\
	if (value == NULL) {\
		die("out of memory");\
	}

enum jam_status {
	JAM_STATUS_ERROR,
	JAM_STATUS_EOF,
	JAM_STATUS_LIST_END,
	JAM_STATUS_OBJECT_END,
	JAM_STATUS_COMMA,
	JAM_STATUS_COLON
};

static struct jam_value *parse_value(
	struct jam_deserializer *deserializer,
	enum jam_status *status,
	size_t nesting_level
);

static char *unexpect(enum jam_status status) {
	switch (status) {
	case JAM_STATUS_EOF:
		return "unexpected end of file";
	case JAM_STATUS_LIST_END:
		return "unexpected bracket";
	case JAM_STATUS_OBJECT_END:
		return "unexpected brace";
	case JAM_STATUS_COMMA:
		return "unexpected comma";
	case JAM_STATUS_COLON:
		return "unexpected colon";
	default:
		return "unexpected error";
	}
}

static struct jam_value *parse_list(
	char *flavor,
	struct jam_deserializer *deserializer,
	enum jam_status *status,
	size_t nesting_level
) {
	struct jam_value *value = jam_list_new(flavor, NULL);
	
	memcheck;
	
	while (true) {
		enum jam_status stat;
		
		struct jam_value *element = parse_value(
			deserializer, &stat, nesting_level + 1
		);
		
		switch (stat) {
		case JAM_STATUS_EOF: // FALLTHROUGH
		case JAM_STATUS_OBJECT_END: // FALLTHROUGH
		case JAM_STATUS_COLON:
			die(unexpect(stat));
		case JAM_STATUS_LIST_END: // FALLTHROUGH
		case JAM_STATUS_COMMA:
			if (element != NULL) {
				bool success = jam_list_push(value, element);
				
				if (!success) {
					die("out of memory");
				}
			}
			
			if (stat == JAM_STATUS_LIST_END) {
				return value;
			} else{
				break;
			}
		default:
			*status = JAM_STATUS_ERROR;
			
			jam_free(value);
			
			return NULL;
		}
	}
}
static struct jam_value *parse_object(
	char *flavor,
	struct jam_deserializer *deserializer,
	enum jam_status *status,
	size_t nesting_level
) {
	struct jam_value *value = jam_object_new(flavor, NULL);
	
	struct jam_value *key = NULL;
	
	memcheck;
	
	while (true) {
		enum jam_status stat;
		
		struct jam_value *element = parse_value(
			deserializer, &stat, nesting_level + 1
		);
		
		switch (stat) {
		case JAM_STATUS_EOF: // FALLTHROUGH
		case JAM_STATUS_LIST_END:
			die(unexpect(stat));
		case JAM_STATUS_COLON:
			if (element == NULL) {
				die("expected an object key");
			}
			
			if (element->flavor[0] != '\0') {
				die("unexpected flavor");
			}
			
			key = element;
			
			break;
		case JAM_STATUS_OBJECT_END: // FALLTHROUGH
		case JAM_STATUS_COMMA:
			if (element == NULL && key != NULL) {
				die("expected an object value");
			}
			
			if (element != NULL) {
				if (key != NULL) {
					if (key->type != JAM_VALUE_STRING) {
						die("invalid object key");
					}
					
					if (jam_object_get(value, key->contents.as_string) != NULL) {
						die("duplicate object member");
					}
					
					bool success = jam_object_set(
						value, key->contents.as_string, element
					);
					
					jam_free(key);
					
					if (!success) {
						die("out of memory");
					}
				} else {
					struct jam_value *children = jam_object_get(
						value, JAM_WORD_CHILDREN
					);
					
					if (children == NULL) {
						bool success = jam_object_set(
							value, JAM_WORD_CHILDREN, jam_list_new("", NULL)
						);
						
						if (!success) {
							die("out of memory");
						}
						
						children = jam_object_get(value, JAM_WORD_CHILDREN);
					} else {
						if (children->type != JAM_VALUE_LIST) {
							die("invalid children container type");
						}
					}
					
					bool success = jam_list_push(children, element);
					
					if (!success) {
						die("out of memory");
					}
				}
			}
			
			key = NULL;
			
			if (stat == JAM_STATUS_OBJECT_END) {
				return value;
			} else{
				break;
			}
		default:
			*status = JAM_STATUS_ERROR;
			
			jam_free(value);
			
			return NULL;
		}
	}
}

static struct jam_value *parse_value(
	struct jam_deserializer *deserializer,
	enum jam_status *status,
	size_t nesting_level
) {
	char *flavor = "";
	struct jam_value *value = NULL;
	
	if (nesting_level >= jam_nesting_limit) {
		die("too much nesting");
	}
	
	while (true) {
		struct jam_token token = jam_next_token(deserializer);
		
		switch (token.type) {
		case JAM_TOKEN_FLAVOR:
			if (flavor[0] != '\0' || value != NULL) {
				if (flavor[0] != '\0' && value == NULL) {
					free(flavor);
				}
				free(token.contents.as_string);
				
				die("unexpected flavor");
			}
			
			flavor = token.contents.as_string;
			
			break;
		case JAM_TOKEN_NULL:
			if (value != NULL) {
				die("unexpected null");
			}
			
			value = jam_null_new(flavor);
			
			memcheck;
			
			break;
		case JAM_TOKEN_BOOL:
			if (value != NULL) {
				die("unexpected boolean");
			}
			
			value = jam_bool_new(flavor, token.contents.as_bool);
			
			memcheck;
			
			break;
		case JAM_TOKEN_NUMBER:
			if (value != NULL) {
				die("unexpected number");
			}
			
			value = jam_number_new(flavor, token.contents.as_number);
			
			memcheck;
			
			break;
		case JAM_TOKEN_STRING:
			if (value != NULL) {
				die("unexpected string");
			}
			
			value = jam_string_new(flavor, token.contents.as_string);
			
			free(token.contents.as_string);
			
			memcheck;
			
			break;
		case JAM_TOKEN_LIST_START:
			if (value != NULL) {
				die("unexpected list");
			}
			
			value = parse_list(flavor, deserializer, status, nesting_level);
			
			if (value == NULL) {
				return NULL;
			}
			
			break;
		case JAM_TOKEN_OBJECT_START:
			if (value != NULL) {
				die("unexpected object");
			}
			
			value = parse_object(flavor, deserializer, status, nesting_level);
			
			if (value == NULL) {
				return NULL;
			}
			
			break;
		case JAM_TOKEN_INVALID:
			*status = JAM_STATUS_ERROR;
			
			return value;
		case JAM_TOKEN_EOF:
			*status = JAM_STATUS_EOF;
			
			return value;
		case JAM_TOKEN_LIST_END:
			*status = JAM_STATUS_LIST_END;
			
			return value;
		case JAM_TOKEN_OBJECT_END:
			*status = JAM_STATUS_OBJECT_END;
			
			return value;
		case JAM_TOKEN_COMMA:
			*status = JAM_STATUS_COMMA;
			
			return value;
		case JAM_TOKEN_COLON:
			*status = JAM_STATUS_COLON;
			
			return value;
		}
	}
	
	return NULL;
}

struct jam_value *jam_get_value(struct jam_deserializer *deserializer) {
	struct jam_value *value = NULL;
	enum jam_status status = JAM_STATUS_ERROR;
	
	value = parse_value(deserializer, &status, 0);
	
	switch(status) {
	case JAM_STATUS_ERROR:
		break;
	case JAM_STATUS_EOF:
		if (value == NULL) {
			value = jam_null_new("");
		}
		
		break;
	default:
		jam_free(value);
		
		deserializer->error = unexpect(status);
		
		return NULL;
	}
	
	return value;
}
