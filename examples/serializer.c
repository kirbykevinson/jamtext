#include <jamtext.h>

int main() {
	struct jam_value *object = jam_object_new("student",
		"name", jam_string_new("", "John Buttocks"),
		
		"alive", jam_bool_new("", true),
		"partner", jam_null_new(""),
		
		"grades", jam_list_new("",
			jam_number_new("", 4),
			jam_number_new("", 2),
			jam_number_new("", 0),
		NULL),
	NULL);
	
	struct jam_serializer *serializer = jam_serializer_new(stdout);
	
	if (object == NULL || serializer == NULL) {
		fprintf(stderr, "error: out of memory");
		
		return 1;
	}
	
	bool success = jam_serializer_run(serializer, object);
	
	if (!success) {
		const char *error = jam_serializer_get_error(serializer);
		
		fprintf(stderr, "error: %s\n", error);
		
		return 1;
	}
	
	jam_free(object);
	free(serializer);
	
	return 0;
}
