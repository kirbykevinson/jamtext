#include <jamtext.h>
#include <string.h>

#define die(message)\
	fprintf(stderr, "error: %s\n", message);\
	exit(1);

struct jam_value *test_start(char *input) {
	struct jam_deserializer *deserializer =
		jam_deserializer_new_from_string(input);
	
	if (deserializer == NULL) {
		die("out of memory");
	}
	
	struct jam_value *value = jam_deserializer_run(deserializer);
	
	if (value == NULL) {
		die("parsing error");
	}
	
	return value;
}

void test_end(bool success, char *name) {
	if (success) {
		printf("Test '%s' passed\n", name);
	} else {
		printf("Test '%s' failed\n", name);
	}
}

void test_flavor(void) {
	struct jam_value *value = test_start("shy null");
	
	bool success = strcmp(value->flavor, "shy") == 0;
	
	test_end(success, "flavor");
}

void test_null(void) {
	struct jam_value *value = test_start("null");
	
	bool success = value->type == JAM_VALUE_NULL;
	
	test_end(success, "null");
}

void test_true(void) {
	struct jam_value *value = test_start("true");
	
	bool success =
		value->type == JAM_VALUE_BOOL &&
		value->contents.as_bool == true;
	
	test_end(success, "true");
}
void test_false(void) {
	struct jam_value *value = test_start("false");
	
	bool success =
		value->type == JAM_VALUE_BOOL &&
		value->contents.as_bool == false;
	
	test_end(success, "false");
}

void test_number(void) {
	struct jam_value *value = test_start("1.7e+6");
	
	bool success =
		value->type == JAM_VALUE_NUMBER &&
		value->contents.as_number == 1.7e+6;
	
	test_end(success, "number");
}

void test_string(void) {
	struct jam_value *value = test_start("\"\\\\\\b\\f\\n\\r\\t\\u0042помогите\nпожалуйста\"");
	
	bool success =
		value->type == JAM_VALUE_STRING &&
		strcmp(
			value->contents.as_string,
			"\\\b\f\n\r\t\x42помогите\nпожалуйста"\
		) == 0;
	
	test_end(success, "string");
}

void test_list(void) {
	struct jam_value *value = test_start("[1, 2, 3]");
	
	bool success =
		value->type == JAM_VALUE_LIST &&
		
		value->contents.as_list != NULL &&
		value->contents.as_list->last != NULL &&
		
		value->contents.as_list->value->contents.as_number == 1 &&
		value->contents.as_list->last->value->contents.as_number == 3;
	
	test_end(success, "list");
}

void test_object(void) {
	struct jam_value *value = test_start("{\"a\": 6, 7}");
	
	struct jam_value *a = NULL, *children = NULL;
	
	bool success =
		value->type == JAM_VALUE_OBJECT &&
		
		value->contents.as_object != NULL &&
		(a = jam_object_get(value, "a")) != NULL &&
		(children = jam_object_get(value, "_children")) != NULL &&
		
		children->type == JAM_VALUE_LIST &&
		children->contents.as_list != NULL &&
		
		a->contents.as_number == 6 &&
		children->contents.as_list->value->contents.as_number == 7;
	
	test_end(success, "object");
}

int main() {
	test_flavor();
	test_null();
	test_true();
	test_false();
	test_number();
	test_string();
	test_list();
	test_object();
	
	return 0;
}
